"use strict";

module.exports = function(sequelize, DataTypes) {
  var Product = sequelize.define("Product", {
    id: { type: DataTypes.INTEGER, autoIncrement: true, primaryKey: true },
    itemId: DataTypes.STRING,
    productId: DataTypes.STRING
  });

  return Product;
};
