"use strict";

module.exports = function(sequelize, DataTypes) {
  var Page = sequelize.define("Page", {
    id: { type: DataTypes.INTEGER, autoIncrement: true, primaryKey: true },
    name: DataTypes.STRING,
    structure: { 
      type: DataTypes.STRING(2048),
      // defaultValue: '[[],[]]',
      get: function() { return JSON.parse(this.getDataValue('structure')) },
      set: function(structureJson) { this.setDataValue('structure', JSON.stringify(structureJson)); }
    },
    skuId: DataTypes.STRING,
    itemId: DataTypes.STRING
  });

  return Page;
};
