"use strict";

module.exports = function(sequelize, DataTypes) {
  var Option = sequelize.define("Option", {
    id: { type: DataTypes.INTEGER, autoIncrement: true, primaryKey: true },
    options: { 
      type: DataTypes.STRING(2048),
      // defaultValue: '[[],[]]',
      get: function() { return JSON.parse(this.getDataValue('options')) },
      set: function(optionsJson) { this.setDataValue('options', JSON.stringify(optionsJson)); }
    },
  });
  return Option;
};
