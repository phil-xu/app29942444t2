"use strict";

module.exports = function(sequelize, DataTypes) {
  var Order = sequelize.define("Order", {
    id: { type: DataTypes.INTEGER, autoIncrement: true, primaryKey: true },
    tId: DataTypes.STRING,
    status: DataTypes.STRING
  });

  return Order;
};
