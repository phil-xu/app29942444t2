
var fs= require('fs')
var request = require('request')
var requestPromise = require('request-promise');
var express = require('express');
var router = express.Router();
var session = '6202b0663e8607ZZ7b4bd07445874269f8500fcbbc56d2c2772610760';
var mlabServer = 'http://mlab-data.leanapp.cn';
// var mlabServer = 'http://localhost:3000';
var models  = require('../models');

var JDClient = require('jos-sdk').JDClient
var js_config = {
	access_token: '42c8a987-a9f9-472d-afa1-97b16f689466',
	app_secret: 'fb8a91c1fc3c445bb222c834c31c3528',
	app_key: '9F13EAC97C66D7675237CE17E0EBDB32',
	format: 'json',
	v: '2.0'
};

function handleAPI(method, params, config) {
	var client = new JDClient(config);
	return new Promise(function(resolve, reject) {
		client.handleAPI(method, params, function (error, results) {
			// results = JSON.parse(results)
			console.log(results)
			if (error) reject(error) 
			else resolve(results)
		})
	})
}

router.get('/detail/:tid', function(req, res, next) {
	var tid = req.params.tid
	var params = {
		'order_id': tid
	};
	var trade = {
		orders: {
			order: []
		}
	}
	handleAPI('360buy.order.get', params, js_config).then(function (results) {
		console.log(results)
		results = JSON.parse(results)
		var orderInfo = results.order_get_response.order.orderInfo
		
		trade.buyer_message = orderInfo.order_remark
		trade.created = orderInfo.order_start_time
		trade.payment = orderInfo.order_payment
		trade.pic_path = ''
		trade.tid = orderInfo.order_id
		trade.title = '京东订单'
		trade.receiver_state = orderInfo.consignee_info.province
		trade.receiver_city = orderInfo.consignee_info.city
		trade.receiver_district = orderInfo.consignee_info.county
		trade.receiver_town = ''
		trade.receiver_address = orderInfo.consignee_info.full_address
		var skuList = []
		for(var i = 0; i < orderInfo.item_info_list.length; i ++) {
			var jdItem = orderInfo.item_info_list[i]
			var order = {}
			order.num = parseInt(jdItem.item_total)
			order.num_iid = jdItem.sku_id
			order.payment = jdItem.jd_price 
			order.title = jdItem.sku_name
			trade.orders.order.push(order)
			var params = {
				'skuId': jdItem.sku_id,
				'field': 'logo'
			};
			skuList.push(function(order) {
				return handleAPI('jingdong.sku.read.findSkuById', params, js_config).then(function(sku) {
					sku = JSON.parse(sku)
					order.pic_path = 'http://img13.360buyimg.com/n1/' + sku.jingdong_sku_read_findSkuById_responce.sku.logo
				})
			}(order))
		}
		return Promise.all(skuList)
	}).then(function(){
		var params = {
			'order_id': tid
		};
		return handleAPI('jingdong.order.venderRemark.queryByOrderId', params, js_config)
	}).then(function(remark){
		console.log(remark)
		remark = JSON.parse(remark)
		trade.seller_memo = remark.jingdong_order_venderRemark_queryByOrderId_responce.venderRemarkQueryResult.vender_remark.remark

		res.send({
			trade: trade
		});
	}).catch(function(e) {
		console.log(e)
		res.send({errorCode: 500});
	})
	// res.send(200);
	// client.handleAPI('360buy.order.get', params, function (error, results) {
	// 	results = JSON.parse(results)
	// 	if (!error) {
	// 		var orderInfo= results.order_get_response.order.orderInfo
	// 		var trade = {
	// 			orders: {
	// 				order: []
	// 			}
	// 		}
	// 		trade.buyer_message = orderInfo.order_remark
	// 		trade.created = orderInfo.order_start_time
	// 		trade.payment = orderInfo.order_payment
	// 		trade.pic_path = ''
	// 		trade.tid = orderInfo.order_id
	// 		trade.title = ''
	// 		trade.receiver_state = orderInfo.consignee_info.province
	// 		trade.receiver_city = orderInfo.consignee_info.city
	// 		trade.receiver_district = orderInfo.consignee_info.county
	// 		trade.receiver_town = ''
	// 		trade.receiver_address = orderInfo.consignee_info.full_address
	// 		for(var i = 0; i < orderInfo.item_info_list.length; i ++) {
	// 			var jdItem = orderInfo.item_info_list[i]
	// 			var order = {}
	// 			order.num = parseInt(jdItem.item_total)
	// 			order.num_iid = jdItem.sku_id
	// 			order.payment = jdItem.jd_price
	// 			order.title = jdItem.sku_name
	// 			trade.orders.order.push(order)
	// 		}
	// 		res.send({
	// 			trade: trade
	// 		})
	// 	} else {
	//     	console.log(error);
	//     	res.send({errorCode: 500});
	//     }
	// }) 
});
function get (page_no, todo, res) {
	var client = new JDClient(js_config);
	var params = {
		'order_state': 'WAIT_SELLER_STOCK_OUT',
		'page': 1,
		'page_size': 10
	};

	client.handleAPI('360buy.order.search', params, function (error, results) {
		console.log(error, results)
		results = JSON.parse(results)
	    if (!error && !results.error_response) {
	    	var trade = results.order_search_response.order_search.order_info_list;
	    	models.Order.findAll({attributes: ['tid', 'status']}).then(function(orders) {
				if(orders.length) {
					var tIdList = orders.map((order) => order.dataValues.tid)
					for (var i = 0; i < trade.length; i++) {
						var t = {
							tid: trade[i].order_id,
							created: trade[i].order_start_time
						}
						console.log(t)
			    		var tId = t.tid + '';
			    		var idx = tIdList.indexOf(tId)	
			    		if (idx >= 0) {
			
			    		} else {
			    			todo.push(t)
			    		}
			    	}
				} 
				res.send({
					trades: {
						trade: todo
					}
				});
				
			}).catch(function(){
				res.send({errorCode: 500});
			});
	    	
	    	
	    } else {
	    	console.log(error);
	    	res.send({errorCode: 500});
	    }
	})
}
router.get('/todo', function(req, res, next) {
	var todo = []
	get(1, todo, res)
});
function list (page_no, result, res) {
	var client = new JDClient(js_config);
	var params = {
		'order_state': 'WAIT_SELLER_STOCK_OUT',
		'page': 1,
		'page_size': 10
	};

	client.handleAPI('360buy.order.search', params, function (error, results) {
		console.log(error, results)
		results = JSON.parse(results)
	    if (!error && !results.error_response) {
	    	var trade = results.order_search_response.order_search.order_info_list;
	    	models.Order.findAll({attributes: ['tid', 'status']}).then(function(orders) {
				if(orders.length) {
					var tIdList = orders.map((order) => order.dataValues.tid)
					for (var i = 0; i < trade.length; i++) {
						var t = {
							tid: trade[i].order_id,
							created: trade[i].order_start_time
						}
						console.log(t)
			    		var tId = t.tid + '';
			    		var idx = tIdList.indexOf(tId)	
			    		if (idx >= 0) {
			    			if (orders[idx].dataValues.status == 'finished') {
			    				result.finished.push(t)
			    			} else if (orders[idx].dataValues.status == 'ignored'){
			    				result.ignored.push(t)
			    			} else {
			    				result.fake.push(t)
			    			}
			    		} else {
			    			result.todo.push(t)
			    		}
			    	}
				} 
				res.send(result);
				
			}).catch(function(){
				res.send({errorCode: 500});
			});
	    	
	    	
	    } else {
	    	console.log(error);
	    	res.send({errorCode: 500});
	    }
	})
}
router.get('/list', function(req, res, next) {
	var result = {
		todo: [],
		ignored: [],
		fake: [],
		finished: []
	}
	list(1, result, res)
});
router.get('/session/:code', function(req, res, next) {
	var code = req.params.code;
	var uri = 'https://oauth.taobao.com/token';
	uri += '?code=' + code;
	uri += '&client_id=' + BACKEND_APP_KEY;
	uri += '&client_secret=' + BACKEND_APP_SECRET;
	uri += '&redirect_uri=' + 'https://customizer.ews.m.jaeapp.com';
	uri += '&grant_type=authorization_code';
	var options = {
		uri: uri,
	    method: 'POST',
	    json: true
	};
	request(options, function(error, response, data) {
	    if (!error && response.statusCode == 200) {
	        console.log(data);
	        session = data.access_token
	        res.send({});
	    } else {
	    	res.send({errorCode: 500});
	    }
	    
	});
});

router.post('/submit', function(req, res, next) {
	console.log(req.body)
	var uri = mlabServer + '/order/anonymous_order';
	var options = {
		uri: uri,
	    method: 'POST',
	    body: req.body,
	    json: true
	};
	request(options, function(error, response, data) {
	    if (!error && response.statusCode == 200) {
	    	models.Order.create({
				tId: req.body.tId,
				status: 'finished'
			}).then(function() {
				res.send({});
			}).catch(function(e){
				res.send({errorCode: 206});
			});
	    } else {
	    	res.send({errorCode: 500});
	    }
	    console.log(error, response, data)
	});	
});
router.get('/ignore/:tId', function(req, res, next) {
	var tId = req.params.tId
	models.Order.create({
		tId: tId,
		status: 'ignored'
	}).then(function() {
		res.send({});
	}).catch(function(e){
		res.send({errorCode: 500});
	});
});
router.get('/fake/:tId', function(req, res, next) {
	var tId = req.params.tId
	models.Order.create({
		tId: tId,
		status: 'fake'
	}).then(function() {
		res.send({});
	}).catch(function(e){
		res.send({errorCode: 500});
	});
});

var provinces = JSON.parse(fs.readFileSync('config/Address_province.json')).results
var cities = JSON.parse(fs.readFileSync('config/Address_city.json')).results
var districts = JSON.parse(fs.readFileSync('config/Address_district.json')).results
router.post('/address', function(req, res, next) {

	var uri = mlabServer + '/address/province/list?total=100';
	var options = {
		uri: uri,
	    json: true
	};
	var province = req.body.receiverState.substring(0, 2)
	var city = req.body.receiverCity.substring(0, 2)
	var district = req.body.receiverDistrict ? req.body.receiverDistrict.substring(0, 2) : '无区'
	//大兴安岭
	if(req.body.receiverDistrict == '大兴区') {
		district = req.body.receiverDistrict
	}

	var address = {
	};
	console.log(province, city, district)
	for (var i = 0; i < provinces.length; i++) {
		var item = provinces[i]
		if (item.name.indexOf(province) == 0) {
			var fields = [{"province":{"objectId":item.objectId,"__type":"Pointer","className":"Address_province"}}];
			options.uri = mlabServer + '/address/city/list?total=100&fields=' + JSON.stringify(fields);
			address.province = {
				objectId: item.objectId,
				__type: 'Pointer',
				className: 'Address_province'
			}
			break
		}
	}
	
	for (var i = 0; i < cities.length; i++) {
		var item = cities[i]
		if (item.name.indexOf(city) == 0) {
			var fields = [{"city":{"objectId":item.objectId,"__type":"Pointer","className":"Address_province"}}];
			options.uri = mlabServer + '/address/district/list?total=100&fields=' + JSON.stringify(fields);
			address.city = {
				objectId: item.objectId,
				__type: 'Pointer',
				className: 'Address_city'
			}
			break
		}
	}
	
	for (var i = 0; i < districts.length; i++) {
		var item = districts[i]
		if (item.name.indexOf(district) == 0) {
			address.district = {
				objectId: item.objectId,
				__type: 'Pointer',
				className: 'Address_district'
			}
			break
		}
	}
	res.send(address)
});

module.exports = router;
