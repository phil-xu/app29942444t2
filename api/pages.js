var models  = require('../models');
var express = require('express');
var router  = express.Router();
var cache = {};

router.post('/', function(req, res) {
	var obj = req.body;
  models.Page.create(obj).then(function(newPage) {

    // cache[obj.itemId] = newPage;
    // console.log(cache);
    res.send({});
  });
});

router.get('/', function(req, res) {
  models.Page.findAll({
    order: [['id', 'DESC']]
	}).then(function(pages) {
		if(pages.length) res.send(pages);
		else res.sendStatus(404);
	});
});

router.put('/:itemId', function(req, res) {
	var itemId = req.params.itemId;
	var obj = req.body;
  models.Page.find({
  	where: {
  		itemId: itemId
  	}
  	
  }).then(function(page) {
		if(page) {
			page.updateAttributes(obj).then(function(newPage){
        cache[itemId] = newPage;
				res.send(newPage);
			})
		} else {
			res.sendStatus(404);
		}
	});
});

router.delete('/:itemId', function(req, res) {
	var itemId = req.params.itemId;
  models.Page.destroy({
    where: {
      itemId: itemId
    }
  }).then(function() {
    delete cache[itemId];
    res.send({});
  });
});

router.get('/:itemId', function(req, res) {
	var itemId = req.params.itemId;
  if(cache[itemId]) {
    res.send(cache[itemId]);
    return;
  }
  models.Page.find({
  	where: {
  		itemId: itemId
  	}
  	
  }).then(function(page) {
		if(page) {
      cache[itemId] = page;
      res.send(page);
    } else {
      res.sendStatus(404);
    }
	});
});

router.get('/clearcache/:itemId', function(req, res) {
  var itemId = req.params.itemId;
  delete cache[itemId];
  res.sendStatus(200);
});
module.exports = router;