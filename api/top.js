
var fs= require('fs')
var request = require('request')
var requestPromise = require('request-promise');
ApiClient = require('@ali/topSdk').ApiClient;
var client = new ApiClient({
	'appkey':'23307822',
	'appsecret':'4d056a5705a5297379dcff80ba052390',
	'url':'http://gw.api.taobao.com/router/rest'
});
var BACKEND_APP_KEY = '24879600';
var BACKEND_APP_SECRET = '11fd28de297b1e095ca8db0d6e2b2f7c';
var backend = new ApiClient({
	'appkey': BACKEND_APP_KEY,
	'appsecret': BACKEND_APP_SECRET,
	'url':'http://gw.api.taobao.com/router/rest'
});
var express = require('express');
var router = express.Router();
var session = '6202b0663e8607ZZ7b4bd07445874269f8500fcbbc56d2c2772610760';
var mlabServer = 'http://mlab-data.leanapp.cn';
// var mlabServer = 'http://localhost:3000';
var models  = require('../models');
router.get('/', function(req, res, next) {

	var content = fs.readFileSync('/Users/phil/morse-yellow.jpg');
	client.execute('taobao.trade.voucher.upload', {
	    'file_name':'morse.jpg',
	    'file_data':content
	}, function(err, response) {
	    if(err){
			res.send(err);
		} else {
			res.send(response);
		}
	})
    
});
router.get('/detail/:tid', function(req, res, next) {
	var tid = req.params.tid || '2729192693574675'; //'2581953629134709';//2508159803564709 2555580445980923 2255446188442350 2581953629134709
	backend.execute('taobao.trade.fullinfo.get', {
	    'fields': 'tid, buyer_nick, title, created, payment, orders, pic_path, buyer_message, buyer_memo, seller_memo, receiver_state, receiver_city, receiver_district, receiver_town, receiver_address, receiver_name, receiver_mobile',
	    'tid': tid,
	    'session': session
	}, function(error, response) {
	    if (!error) res.send(response);
	    else res.send({errorCode: 500});
	})
});
function get (page_no, todo, res) {
	backend.execute('taobao.trades.sold.get', {
	    'fields':'tid,title,created,pay_time,payment,pic_path',
	    'page_size': 100,
	    'type':'guarantee_trade,auto_delivery,ec,cod,step,nopaid',
	    'page_no': page_no,
	    'use_has_next': true,
	    'status': 'WAIT_SELLER_SEND_GOODS', // 'WAIT_SELLER_SEND_GOODS',//'WAIT_BUYER_PAY',
	    'session': session
	}, function(error, response) {

	    if (!error) {
				// console.log(response)
	    	if (response.has_next == false && !response.trades) {
					response.trades = {}
					
					response.trades.trade = todo
					// console.log(11, response)
					res.send(response);
	    		return
	    	}
	    	var trade = response.trades.trade;
	    	// console.log('1', response.trades.trade.length)
	    	// var todo = [];
	    	models.Order.findAll({attributes: ['tid']}).then(function(orders) {
				
				if(orders.length) {
					var tIdList = orders.map((order) => order.dataValues.tid)
					
					for (var i = 0; i < trade.length; i++) {
			    		var tId = trade[i].tid + '';
			    		
			    		if (tIdList.indexOf(tId) >= 0) {
			    		} else {
			    			todo.push(trade[i])
			    		}
			    	}
					// response.trades.trade = todo
				}
				if (response.has_next) {
					get(page_no + 1, todo, res)
				} else {
					// console.log('10011', todo)
					response.trades.trade = todo
					// console.log('2', response.trades.trade.length)
					res.send(response);
				}
			}).catch(function(){
				res.send({errorCode: 500});
			});
	    } else {
	    	console.log(error);
	    	if (error.code == 27) res.send({errorCode: 403});
	    	else res.send({errorCode: 500});
	    }
	})
}
router.get('/todo', function(req, res, next) {
	var todo = []
	get(1, todo, res)
});
function list (page_no, result, res) {
	backend.execute('taobao.trades.sold.get', {
	    'fields':'tid,title,created,pay_time,payment,pic_path',
	    'type':'guarantee_trade,auto_delivery,ec,cod,step,nopaid',
	    'page_size': 100,
	    'page_no': page_no,
	    'use_has_next': true,
	    'status': 'WAIT_SELLER_SEND_GOODS', // 'WAIT_SELLER_SEND_GOODS',//'WAIT_BUYER_PAY',
	    'session': session
	}, function(error, response) {
	    if (!error) {
				console.log(1, response)
	    	if (response.has_next == false && !response.trades) {
	    		res.send(result);
	    		return
	    	}
	    	var trade = response.trades.trade;
	    	models.Order.findAll({attributes: ['tid', 'status']}).then(function(orders) {
				if(orders.length) {
					var tIdList = orders.map((order) => order.dataValues.tid)
					for (var i = 0; i < trade.length; i++) {
			    		var tId = trade[i].tid + '';
			    		var idx = tIdList.indexOf(tId)	
			    		if (idx >= 0) {
			    			if (orders[idx].dataValues.status == 'finished') {
			    				result.finished.push(trade[i])
			    			} else if (orders[idx].dataValues.status == 'ignored'){
			    				result.ignored.push(trade[i])
			    			} else {
			    				result.fake.push(trade[i])
			    			}
			    		} else {
			    			result.todo.push(trade[i])
			    		}
			    	}
				} 
				if (response.has_next) {
					list(page_no + 1, result, res)
				} else {
					res.send(result);
				}
				// res.send(result);
				
			}).catch(function(){
				res.send({errorCode: 500});
			});
	    	
	    	
	    } else {
	    	console.log(error);
	    	if (error.code == 27) res.send({errorCode: 403});
	    	else res.send({errorCode: 500});
	    }
	})
}
router.get('/list', function(req, res, next) {
	var result = {
		todo: [],
		ignored: [],
		fake: [],
		finished: []
	}
	list(1, result, res)
});
router.get('/session/:code', function(req, res, next) {
	var code = req.params.code;
	var uri = 'https://oauth.taobao.com/token';
	uri += '?code=' + code;
	uri += '&client_id=' + BACKEND_APP_KEY;
	uri += '&client_secret=' + BACKEND_APP_SECRET;
	uri += '&redirect_uri=' + 'https://customizer.ews.m.jaeapp.com';
	uri += '&grant_type=authorization_code';
	var options = {
		uri: uri,
	    method: 'POST',
	    json: true
	};
	console.log(uri)
	request(options, function(error, response, data) {
			console.log(error, response.statusCode)
	    if (!error && response.statusCode == 200) {
	        console.log(data);
	        session = data.access_token
	        res.send({});
	    } else {
	    	res.send({errorCode: 500});
	    }
	    
	});
});

router.post('/submit', function(req, res, next) {
	console.log(req.body)
	var uri = mlabServer + '/order/anonymous_order';
	var options = {
		uri: uri,
	    method: 'POST',
	    body: req.body,
	    json: true
	};
	request(options, function(error, response, data) {
	    if (!error && response.statusCode == 200) {
	    	models.Order.create({
				tId: req.body.tId,
				status: 'finished'
			}).then(function() {
				res.send({});
			}).catch(function(e){
				res.send({errorCode: 206});
			});
	    } else {
	    	res.send({errorCode: 500});
	    }
	    console.log(error, response, data)
	});	
});
router.get('/ignore/:tId', function(req, res, next) {
	var tId = req.params.tId
	models.Order.create({
		tId: tId,
		status: 'ignored'
	}).then(function() {
		res.send({});
	}).catch(function(e){
		res.send({errorCode: 500});
	});
});
router.get('/fake/:tId', function(req, res, next) {
	var tId = req.params.tId
	models.Order.create({
		tId: tId,
		status: 'fake'
	}).then(function() {
		res.send({});
	}).catch(function(e){
		res.send({errorCode: 500});
	});
});
var provinces = JSON.parse(fs.readFileSync('config/Address_province.json')).results
var cities = JSON.parse(fs.readFileSync('config/Address_city.json')).results
var districts = JSON.parse(fs.readFileSync('config/Address_district.json')).results
router.post('/address', function(req, res, next) {

	var uri = mlabServer + '/address/province/list?total=100';
	var options = {
		uri: uri,
	    json: true
	};
	var province = req.body.receiverState.substring(0, 2)
	var city = req.body.receiverCity.substring(0, 2)
	var district = req.body.receiverDistrict ? req.body.receiverDistrict.substring(0, 2) : '无区'
	//大兴安岭
	if(req.body.receiverDistrict == '大兴区') {
		district = req.body.receiverDistrict
	}

	if(req.body.receiverState.length == 2) {
		province = city
		city = district
	}
	var address = {
	};
	
	for (var i = 0; i < provinces.length; i++) {
		var item = provinces[i]
		if (item.name.indexOf(province) == 0) {
			var fields = [{"province":{"objectId":item.objectId,"__type":"Pointer","className":"Address_province"}}];
			options.uri = mlabServer + '/address/city/list?total=100&fields=' + JSON.stringify(fields);
			address.province = {
				objectId: item.objectId,
				__type: 'Pointer',
				className: 'Address_province'
			}
			break
		}
	}
	
	for (var i = 0; i < cities.length; i++) {
		var item = cities[i]
		if (item.name.indexOf(city) == 0) {
			var fields = [{"city":{"objectId":item.objectId,"__type":"Pointer","className":"Address_province"}}];
			options.uri = mlabServer + '/address/district/list?total=100&fields=' + JSON.stringify(fields);
			address.city = {
				objectId: item.objectId,
				__type: 'Pointer',
				className: 'Address_city'
			}
			break
		}
	}
	
	for (var i = 0; i < districts.length; i++) {
		var item = districts[i]
		if (item.name.indexOf(district) == 0) {
			address.district = {
				objectId: item.objectId,
				__type: 'Pointer',
				className: 'Address_district'
			}
			break
		}
	}
	res.send(address)
});

module.exports = router;
