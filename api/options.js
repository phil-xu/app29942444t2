var models  = require('../models');
var express = require('express');
var router = express.Router();

router.get('/:id', function(req, res, next) {
	var id = req.params.id;
	models.Option.find({
		where: {
			id: id
		}
	}).then(function(options) {
		if(options) {
			res.send(options);
		} else {
			res.send({errorCode: 404});
		}
	}).catch(function(e){
		res.send(e);
	});
});

router.post('/', function(req, res, next) {
	console.log(req.body)
	models.Option.create(req.body).then(function(option) {
		res.send(option);
	}).catch(function(e){
		res.send({errorCode: 500});
	});
});

module.exports = router;
