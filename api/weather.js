var express = require('express');
var path = require('path');
var router = express.Router();
var moment = require('moment')
var DarkSkyApi = require('dark-sky-api');
// var darksky = new DarkSky('df3dcbb98e967a076a0f93f9eef33627')
// var options = {
//   APIKey: 'df3dcbb98e967a076a0f93f9eef33627',
//   timeout: 10000
// }
// var darksky = new DarkSky(options)
DarkSkyApi.apiKey = 'df3dcbb98e967a076a0f93f9eef33627'
DarkSkyApi.proxy = true;
DarkSkyApi.units = 'ca'; // default 'us'
DarkSkyApi.language = 'zh'; // default 'en'
router.post('/', function(req, res, next) {
	// darksky
 //    .options({
 //        latitude: req.body.lat,
 //        longitude: req.body.lng,
 //        time: moment(req.body.timestamp).format('YYYY-MM-DD') ,
 //        language: 'zh',
 //        exclude: ['currently', 'minutely', 'hourly', 'flags', 'alerts']
 //    })
 //    .units('ca')
 //    .get()
 //    .then(r => {
 //    	res.send(r)
 //    }, e => {
 //    	res.status(500)
 //    })
 //    .catch(e => {
 //    	res.status(500)
 //    })
    // darksky.getAtTime(req.body.lat, req.body.lng, Math.floor(req.body.timestamp / 1000 ), function (err, response, data) {
    //     console.log(err, response, data)
    //     res.send(response)
    // });
    DarkSkyApi.loadTime(moment(req.body.timestamp).format(), {
        latitude: req.body.lat,
        longitude: req.body.lng,
        language: 'zh',
    }).then(result => {
        res.send(result)
    }, e => {
        console.log(e)
        res.status(500)
    }).catch(e => {
        console.log(e)
        res.status(500)
    })
});
module.exports = router;

