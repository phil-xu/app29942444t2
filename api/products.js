var models  = require('../models');
var express = require('express');
var router = express.Router();
var request = require('request')
router.get('/detail/:id', function(req, res, next) {
	var id = req.params.id;
	var uri = 'http://mlab-data.leanapp.cn/product/data/' + id;
	var options = {
		uri: uri,
	    method: 'GET',
	    json: true
	};
	request(options, function(error, response, data) {
	    if (!error && response.statusCode == 200) {
	    	var info = {};
	    	info.config = data.model.objData;
	    	info.title = data.name + data.subname;
	    	info.price = data.priceCount.js;
	    	info.objectId = data.objectId;
            for (var i in data.images) {
                info.pic = 'http://cdn.m-lab.cn/' + data.images[i]['main'];
                break;
            }
	        res.send(info);
	       
	    } else {
	    	res.send({errorCode: 404});
	    }
	});	
});

router.get('/id/:itemId', function(req, res, next) {
	var itemId = req.params.itemId;
	models.Product.find({
		where: {
			itemId: itemId
		}
	}).then(function(product) {
		if(product) {
			res.send(product);
		} else {
			res.send({errorCode: 404});
		}
	}).catch(function(e){
		res.send(e);
	});
});

router.post('/id', function(req, res, next) {
	models.Product.create(req.body).then(function(newPage) {
		res.send({});
	}).catch(function(e){
		res.send(e);
	});
});

module.exports = router;
